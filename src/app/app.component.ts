import {Component} from '@angular/core';
import {ClassificationService} from './classification.service';
import {HttpEventType, HttpProgressEvent} from '@angular/common/http';
import {ClassificationResults} from './model/classification-results';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'Pneumonia Detector';
  public classificationResults: ClassificationResults = null;
  public selectedFile: File = null;
  public selectedFileUrl: any = null;
  public dropZoneHovered = false;

  constructor(private classificationService: ClassificationService, public sanitizer: DomSanitizer) { }

  public handleDrop(file: File){
    this.selectedFile = file;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.selectedFileUrl = reader.result;
    };
  }

  public handleHover($event: boolean) {
    this.dropZoneHovered = $event;
    console.log($event);
  }

  public classify() {
    this.classificationService.classify(this.selectedFile).subscribe(
      (response) => {
        switch (response.type) {
          case HttpEventType.UploadProgress:
            this.showProgress(response);
            break;
          case HttpEventType.Response:
            const results: ClassificationResults = {...response.body};
            this.showResults(results);
        }
      }
    );
  }

  public cancel() {
    this.selectedFile = null;
    this.selectedFileUrl = null;
    this.dropZoneHovered = false;
  }

  private showProgress(response: HttpProgressEvent) {
    console.log(100 * response.loaded / response.total);
  }

  public showResults(results: ClassificationResults) {
    this.classificationResults = results;
    console.log(this.classificationResults);
  }

  public reset() {
    this.classificationResults = null;
    this.selectedFile = null;
    this.selectedFileUrl = null;
  }
}
