export interface ClassificationResults {
  mean: number;
  variance: number;
  heatmap: string;
}
