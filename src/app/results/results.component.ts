import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ClassificationResults} from '../model/classification-results';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.sass']
})
export class ResultsComponent implements OnChanges {
  @Output() newClassification = new EventEmitter();
  @Input()
  public classificationResults: ClassificationResults = null;
  public progressBarValue = 0;
  constructor(public sanitizer: DomSanitizer) {
  }

  public handleNewClassificationClicked() {
    this.newClassification.emit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const val = changes.classificationResults.currentValue.mean * 100;
    const time = 500;
    const timestep = time / 50;
    const d = val / timestep;

    const anim = (next, diff, max) => {
      this.progressBarValue = next;
      if (next < max) {
        setTimeout(() => anim(next + diff, diff, max), timestep);
      }
    };
    setTimeout(() => anim(d, d, val), 0);
  }
}
