import {Directive, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appFileDrop]'
})
export class FileDropDirective {
  @Output() filesDropped = new EventEmitter<any>();

  @Output() filesHovered = new EventEmitter<boolean>();

  @HostListener('dragover', ['$event']) dragOver(event) {
    event.preventDefault();
    event.stopPropagation();
    this.filesHovered.emit(true);
  }

  @HostListener('dragleave', ['$event']) public dragLeave(event) {
    event.preventDefault();
    event.stopPropagation();
    this.filesHovered.emit(false);
  }

  @HostListener('drop', ['$event']) public drop(event) {
    event.preventDefault();
    event.stopPropagation();
    this.filesHovered.emit(false);
    const files = event.dataTransfer.files;
    if (files.length > 0) {
      this.filesDropped.emit(files[0]);
    }
  }
}

