import {Injectable} from '@angular/core';
import {HttpClient, HttpEventType, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';
import {map} from 'rxjs/operators';
import {ClassificationResults} from './model/classification-results';

@Injectable({
  providedIn: 'root'
})
export class ClassificationService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  public classify(file: File) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post<ClassificationResults>(this.apiUrl + '/classify', formData, {
      reportProgress: true,
      observe: 'events'
    });
  }
}
